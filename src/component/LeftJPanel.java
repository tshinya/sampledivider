package component;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

public class LeftJPanel extends JPanel {

	public LeftJPanel() {
		this.setBackground(Color.BLUE);
		this.setMinimumSize(new Dimension(100, 100));
		this.setVisible(true);
	}
}
