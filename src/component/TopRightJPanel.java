package component;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

public class TopRightJPanel extends JPanel {

	public TopRightJPanel() {
		this.setBackground(Color.RED);
		this.setMinimumSize(new Dimension(100, 100));
		this.setVisible(true);
	}
}
