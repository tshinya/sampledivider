package component;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

public class BottomRightJPanel extends JPanel {

	public BottomRightJPanel() {
		this.setMinimumSize(new Dimension(100, 100));
		this.setBackground(Color.GREEN);
		this.setVisible(true);
	}
}
