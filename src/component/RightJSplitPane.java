package component;

import java.awt.Dimension;
import javax.swing.JSplitPane;

public class RightJSplitPane extends JSplitPane {
	
	public RightJSplitPane() {
		super(VERTICAL_SPLIT);
		this.add(new TopRightJPanel(), TOP);
		this.add(new BottomRightJPanel(), BOTTOM);
		setMinimumSize(new Dimension(100, 100));
		this.setVisible(true);
	}

}
