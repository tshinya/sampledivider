package main;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import component.LeftJPanel;
import component.RightJSplitPane;

public class MainFrame extends JFrame {
	
	public MainFrame() {
		super("DividerSample");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		splitPane.add(new LeftJPanel(), JSplitPane.LEFT);
		splitPane.add(new RightJSplitPane(), JSplitPane.RIGHT);
		this.add(splitPane);
	}

}
